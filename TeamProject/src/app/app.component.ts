import { Component } from '@angular/core';
import * as $ from 'jquery';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})





export class AppComponent {

  	changeDropdowns(selectedValue: string, optionId: number): void{

  		for(var i = 1; i <= 4; i++){
  			if(i != optionId){
  				$("#option" + i + " option[value='" + selectedValue + "']").remove();
  			}
  		}  		
	}

	resetForm(): void{
		var choices = ["<option value='default'>Please Select...</option>", "<option value='1'>1. Best time</option>", "<option value='2'>2. Next Best time</option>", "<option value='3'>3. Not as good</option>", "<option value='4'>4. Worst time</option>"];

		for(var t = 1; t <= 4; t++){
			$("#option" + t).empty();
			for(var v = 0; v < 5; v++){
				$("#option" + t).append(choices[v]);
			}
		}
	}

}


